import React from 'react'

import { Redirect, Route } from 'react-router-dom'

const PrivateRoute = ({ Component, path }) => {
    return (  
        <Route 
            path={path}
            render={(props) =>
                <Component {...props}/>
            } 
        />
    );

}
 
export default PrivateRoute;